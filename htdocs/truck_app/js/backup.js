
// ------------------------------------------------------------- //
//                      JQuery UI dialogs
// ------------------------------------------------------------- //
$(function(){
  //New entry variables
  var tag_id = (Math.floor(Math.random()*Date.now()))/10000, truck = $("#Truck"), date_1 = $("#Date"), tag_des = $("#Tag_description"), done = 0;
  //Sign off variables
  var kms = $("#Kilometers"), date_2 = $("#Date_2");
  //Tag sign off variables
  var tag_kms = $("#Kilometers_tag"), tag_date = $("#Date_3");
  var td_val_done = 0;
  var job_number = 0;

  //Add tag variables
  var vid_val = 0;
  var tag_date = $(add_Date)

  //Define the dialog windows' properties
// ------ New Record Dialog ------ //
  $("#new_entry_dialog").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
       "Add Record": function(){
         var T_id = parseInt(truck.val());
         //Add items to the POST request and send
         $.post('db_comms.php', {T_ID : T_id,
                                 DATE : date_1.val(),
                                 TAG : tag_des.val(),
                                 TAG_ID : tag_id,
                                 DONE : done},
                                 function(result){
                                   console.log(result);
                                 });
         //Debug console
         console.log("T_ID:"+truck.val());
         console.log("Date:"+date_1.val());
         console.log("Tag:"+tag_des.val());
         console.log("TAG_ID:"+tag_id);
         // console.log(type(tag_des.val()));
         $("#new_entry_dialog").dialog( "close" );

         // window.location.reload();
       },
       //Close dialog window
       Cancel: function() {$("#new_entry_dialog").dialog("close");}
     },
     //Close dialog window
     close: function() {$("#new_entry_dialog").dialog("close");}
  });

// ------ Sign Off Dialog ------ //
  $("#dialog_signoff").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Add Details": function(){
        $.post('db_comms.php', {KMS: kms.val(),
                                COMP_DATE : date_2.val(),
                                JOB : td_val_done},
                                function(result){
                                  console.log(result);
                                  console.log(td_val_done);
                                });
        console.log("Odometer:"+kms.val());
        console.log("Completed Date:"+date_2.val());
        $("#dialog_signoff").dialog("close");
        window.location.reload();
      },
      Cancel: function(){$("#dialog_signoff").dialog("close");}
    },
    close: function(){$("#dialog_signoff").dialog("close");}
  });

// ------ Tag signoff Dialog ------ //
  $("#tag_dialog_signoff").dialog({
    autoOpen : false,
    modal: true,
    closeOnEscape: true,
    buttons: {
      "Add Details": function(){
        $.post('db_comms.php',{TAGKMS: tag_kms.val(),
                               TAGDATE: tag_date.val()},
                               function(result){
                                 console.log(result);
                             });
         $("#tag_dialog_signoff").dialog("close");
         window.location.reload();
      },
      Cancel: function(){$("#tag_dialog_signoff").dialog("close");}
    },
    close: function(){$("#tag_dialog_signoff").dialog("close");}
  })

// ------ Tag button Dialog ------ //
  $("#tag_dialog").dialog({
    autoOpen: false,
    modal: true,
    resizeble: true,
    closeOnEscape: true,
    open: function(event, ui){
          $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
      },
      buttons:{
        "Add tag": function(){
          $.post('db_comms.php',{ VID: vid_val,
                                  DATE:

          })
        }
      }

  })

// ------ Add description Dialog ------ //
  $("#new_tag_dialog").dialog({
    autoOpen: false,
    modal: true,
    resizeble: true,
    closeOnEscape: true,
    open: function(event, ui){
          $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
      }
  })
//"New Entry" button click event
  $("#new_entry").click(function(){
    $("#new_entry_dialog").dialog("open");
  })

//"Add Description" button click event
  $("tr").on("click",  "#Add_des", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    var td_val = rowIndex.find("td:eq(0)").text();

    vid_val = parseInt(rowIndex.find("td:eq(1)").text());

    console.log("Add button pressed with VID:" + vid_val);
    //Get description to be added
    // var des = ', '+window.prompt("Add a description");
    // Post description to update table
    $("#new_tag_dialog").dialog("open");

    // $.post('db_comms.php', {ADD : des,
    //                         ROW : td_val},
    //                         function(result){
    //                           console.log(result);
    //                         });

    // window.location.reload();
  });

//"Delete" buttons click events
  $("tr").on("click",  "#Delete", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    var td_val = rowIndex.find("td:eq(0)").text();
    //Post deletion details to delete values from diary table
    $.post('test.php', {DEL : des,
                        ROW : td_val},
                        function(result){
                          console.log(result);
                          // alert(result);
                        });

    window.location.reload();
  });

//"Done" button click event
  $("tr").on("click", "#Done", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    td_val_done = rowIndex.find("td:eq(0)").text();
    //Launch the dialog form
    $("#dialog_signoff").dialog("open");

  })

//"Tag" button click event
  $("tr").on("click", "#Tag", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    td_val_done = rowIndex.find("td:eq(0)").text();

    $("#tag_dialog").dialog("open");
  })

//"Refresh" button click event
  $("#refresh_btn").click(function(){
    window.location.reload();
  })
//"Tag_delete" button click event
  $("#Tag_delete").click(function(){
      // window.location.reload();
  })
//"Tag_signoff" button click event
  $("#Tag_signoff").click(function(){
      // window.location.reload();
  })
})
