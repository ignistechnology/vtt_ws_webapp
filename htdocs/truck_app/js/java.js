   
// ------------------------------------------------------------- //
//                      JQuery UI dialogs
// ------------------------------------------------------------- //
$(function(){
  //Add details variables
  var job_num = $('#Job_num'), truck = $("#Truck"), prob = $("#Problem"), date_1 = $("#Date"), comms = $("#Comments");
  var kms = $("#Kilometers"), date_2 = $("#Date_2");
  var td_val_done = 0;
  var job_number = 0;

  //Define the dialog windows' properties
// ------ New Record Dialog ------ //
  $("#dialog-form").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
       "Add Record": function(){
         //Add items to the POST request and send
         $.post('test.php', {JOB_NUM : job_num.val(), T_ID : truck.val(), PROB : prob.val(), DATE : date_1.val(), COMMS : comms.val()}, function(result){alert(result);});
         //Debug console
         console.log("JOB_NUM:"+job_num.val());
         console.log("T_ID:"+truck.val());
         console.log("Problem:"+prob.val());
         console.log("Date:"+date_1.val());
         console.log("Comms:"+comms.val());
         $("#dialog-form").dialog( "close" );
       },
       //Close dialog window
       Cancel: function() {$("#dialog-form").dialog("close");}
     },
     //Close dialog window
     close: function() {$("#dialog-form").dialog("close");}
  });

// ------ Sign Off Dialog ------ //
  $("#dialog_signoff").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
      "Add Details": function(){
        $.post('test.php', {KMS: kms.val(), COMP_DATE : date_2.val(), JOB : td_val_done}, function(result){alert(result);});
        console.log("Odometer:"+kms.val());
        console.log("Completed Date:"+date_2.val());
        $("#dialog_signoff").dialog("close");
      },
      Cancel: function(){$("#dialog_signoff").dialog("close");}
    },
    close: function(){$("#dialog_signoff").dialog("close");}
  });

//"New Entry" button click event
  $("#new_entry").click(function(){
    $("#dialog-form").dialog("open");
  })

//"Add Description" button click event
  $("tr").on("click",  "#Add_des", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    // var rowIndex = $(this).parent().parent().index('#diary tbody tr');
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    var td_val = rowIndex.find("td:eq(0)").text();
    //Get description to be added
    var des = ', '+window.prompt("Add a description");
    //Post description to update table
    $.post('test.php', {ADD : des, ROW : td_val}, function(result){alert(result);});
  });

//"Delete" buttons click events
  $("tr").on("click",  "#Delete", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    var td_val = rowIndex.find("td:eq(0)").text();
    //Post deletion details to delete values from diary table
    $.post('test.php', {DEL : des, ROW : td_val}, function(result){alert(result);});

  });

//"Done" button click event
  $("tr").on("click", "#Done", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    td_val_done = rowIndex.find("td:eq(0)").text();
    //Launch the dialog form
    $("#dialog_signoff").dialog("open");

  })
});
