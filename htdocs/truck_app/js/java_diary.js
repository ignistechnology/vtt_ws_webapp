// ------------------------------------------------------------- //
//                      JQuery UI dialogs
// ------------------------------------------------------------- //
$(function(){
// ---------- Variables ----------//
  //New Entry Variables
  var new_VID = $('#new_VID'), new_Tag_Des = $('#new_Tag_Des'), new_Tag_Date = $('#new_Date'), new_Tag_ID = (Math.floor(Math.random()*Date.now()))/10000;

  //Add Tag Variables
  var vid_val = 0, add_Tag_des = $('#add_Tag_Des'), add_Tag_Date = $('#add_Date'), add_Tag_ID = (Math.floor(Math.random()*Date.now()))/10000;

  //Tag Buttons Variables
  var tag_vid_val = 0, tag_text, Tag_id_text, Date_text, today;

  //Sign off variables
  var sign_off_date = $("#Date_sign_off"), odometer = $("#Kilometers_sign_off");


// ---------- UI Dialog Definitions ----------//
  //New Entry UI Dialog
  $("#new_entry_dialog").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
       "Add Record": function(){
         var T_id = parseInt(new_VID.val());
         //Add items to the POST request and send
         $.post('db_comms.php', {T_ID : T_id,
                                 TAG: new_Tag_Des.val(),
                                 DATE: new_Tag_Date.val(),
                                 TAG_ID: new_Tag_ID},
                                 function(result){
                                   console.log(result);
                                 });
         $("#new_entry_dialog").dialog("close");
         window.location.reload();
       },
       //Close dialog window
       Cancel: function() {$("#new_entry_dialog").dialog("close");}
     },
     //Close dialog window
     close: function() {$("#new_entry_dialog").dialog("close");}
  });

  //Add Tag UI Dialog
  $("#new_tag_dialog").dialog({
    autoOpen: false,
    modal: true,
    buttons: {
       "Add Tag": function(){
         //Add items to the POST request and send
         $.post('db_comms.php', {ADD_VID: vid_val,
                                 ADD_DES: add_Tag_des.val(),
                                 ADD_DATE: add_Tag_Date.val(),
                                 ADD_ID: add_Tag_ID,
                                 DONE: 0},
                                 function(result){
                                   console.log(result);
                                 });
         $("#new_tag_dialog").dialog("close");
         window.location.reload();
       },
       //Close dialog window
       Cancel: function() {$("#new_tag_dialog").dialog("close");}
     },
     //Close dialog window
     close: function() {$("#new_tag_dialog").dialog("close");}
  });

  //Tag Dialog
  $("#tag_dialog").dialog({
    autoOpen: false,
    modal: true,
    resizeble: true,
    closeOnEscape: true
  })

  $("#tag_dialog_signoff").dialog({
    autoOpen: false,
    modal: true,
    resizeble: true,
    closeOnEscape: true,
    buttons:{
      "Sign Off": function(){
        //Get odometer value
        var odo = odometer.val();
        $.post('db_comms.php',{SIGN: 1,
                               VID: tag_vid_val,
                               DATE_CR: Date_text,
                               DATE_CO: today,
                               DESCR: tag_text,
                               TAG: Tag_id_text,
                               ODO: odo},
                               function(result){
                                 console.log(result);
                               });
       },
       //Close dialog window
       Cancel: function(){$("#tag_dialog_signoff").dialog("close");}
     },
    //Close dialog window
     close: function(){$("#tag_dialog_signoff").dialog("close");}
  });

// ---------- Button Click Events ----------//
  //New Entry Button Click
  $("#new_entry").click(function(){
    $("#new_entry_dialog").dialog("open");
  });

  //Add Tag Button Click
  $("tr").on("click",  "#Add_des", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get value in the table cell - to corelate with the job number in the table
    var td_val = rowIndex.find("td:eq(3)").text();
    //Determine the vehicle ID
    vid_val = parseInt(rowIndex.find("td:eq(0)").text());
    //Open the UI dialog for insertion of information
    $("#new_tag_dialog").dialog("open");
  });

  //Tag Buttons Click
  $("tr").on("click",  "#tag_des", function(event){
    //Limit event propagation to the current row
    event.stopPropagation();
    //Tag's element number
    var Item = $(this).index();
    //Get row index of table
    var rowIndex = $(this).closest("tr");
    //Get the children in the TAG_ID column
    var column_tag = rowIndex.find("td:eq(3)").children();
    //Get tge children in the DATE_CREATED column
    var column_date = rowIndex.find("td:eq(1)").children();
    //Get the tag's text
    tag_text = $(this).text();
    //Select the HTMl content of the child with the index of Item (TAG ID)
    Tag_id_text = parseInt(column_tag[Item].innerHTML);
    //Select the HTMl content of the child with the index of Item (DATE_CREATED)
    Date_text = column_date[Item].innerHTML;
    //Determine the vehicle ID
    tag_vid_val = parseInt(rowIndex.find("td:eq(0)").text());
    //Get the current date
    today = new Date(), dd = today.getDate(), mm = today.getMonth()+1, yyyy = today.getFullYear();
    if(dd < 10){dd='0'+dd;}
    if(mm < 10){mm="0"+mm;}
    today = yyyy+'-'+mm+'-'+dd;
    //Console logs for debugging
    console.log("Tag text: "+tag_text);
    console.log("Vehicle ID: "+tag_vid_val);
    console.log("Element number: "+Item);
    console.log("Corresponding Tag ID: "+Tag_id_text);
    console.log("Corresponding Date Created: "+Date_text);
    console.log("Current date: "+today);
    //Open the UI dialog for insertion of information
    $("#tag_dialog").dialog("open");
  });
  //Tag Dismiss Button Click
  $("#Tag_dismiss").click(function(){
    //Send the tag to the "doesnt matter" database
    $.post('db_comms.php',{DISS: 1,
                           VID: tag_vid_val,
                           DATE_CR: Date_text,
                           DATE_CO: today,
                           DESCR: tag_text,
                           TAG: Tag_id_text},
                           function(result){
                             console.log(result);
                           });
   //Close the tag dialog window
   $("#tag_dialog").dialog("close");
   //Refresh page
   window.location.reload();
  });
  //Tag Delete Button Click
  $("#Tag_delete").click(function(){
    //Delete tag from current database
  });
  //Tag Sing Off Button Click
  $("#Tag_signoff").click(function(){
    //sign off on current tag
    //Send tag to service records
    $("#tag_dialog_signoff").dialog("open");
  });

})
