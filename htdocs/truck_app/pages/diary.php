<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style_new.css">
    <!-- Bootstrap link -->
    <link href="../bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font -->
    <link href="../public/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- MetisMenu -->
    <link href="../public/metisMenu/metisMenu.min.css" rel="stylesheet">
    <style>
      label, input {display: block;}
      fieldset{padding:0; border:0; margin-top:25px;}
    </style>
    <title>Jobs Page</title>
  </head>
  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
          <a class="navbar-brand" href="diary.php">Van Tonder Transport</a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
          <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                  <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
                  <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
                  <li class="divider"></li>
                  <li>
                    <a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                  </li>
              </ul>

          </li>
        </ul>
        <div class="navbar-default sidebar" role="navigation">
          <div class="sidebar-nav navbar-collapse">
              <ul class="nav" id="side-menu">
                <li>
                  <a href="admin.html"><i class="fa fa-dashboard fa-fw"></i> Admin</a>
                </li>
                <li>
                  <a href="diary.php"><i class="fa fa-table fa-fw"></i> Open Jobs</a>
                </li>
                <li>
                  <a href="#"><i class="fa fa-bar-chart-o fa-caret-down fa-fw" id="serv_rec"></i> Service Records</a>
                  <div class="drop_container">
                    <ul class="nav nav-second-level">
                      <li>
                        <a href="services.php"> Service</a>
                      </li>
                      <li>
                        <a href="maintenance.php"> Maintenance</a>
                      </li>
                      <li>
                        <a href="warranty.php"> Warranty</a>
                      </li>
                      <li>
                        <a href="fines.php"> Fines</a>
                      </li>
                    </ul>
                  </div>
                </li>
                <li>
                    <a href="tyres.php"><i class="fa fa-dot-circle-o fa-fw"></i> Tyres</a>
                </li>
                <li>
                    <a href="brakes.php"><i class="fa fa-ban fa-fw"></i> Brakes</a>
                </li>                                
              </ul>
          </div>
      </div>

    </nav>

      <div id="page-wrapper">
        <div class="container-fluid">
          <div class="row">
              <div class="col-lg-12">
                  <br>
              </div>
          </div>
          <div class="col-lg-12">
              <div class="panel panel-default">
                  <div class="panel-heading">
                      Open Jobs
                      <button type="button" name="refresh_btn" id="refresh_btn" class="btn btn-default btn-sm">Refresh</button>
                  </div>
                  <div class="panel-body">
                      <span id = "alertpane">
                      </span>
                        <?php
                          $_CONNECTION    = mysqli_connect("localhost","root","","testdb") or die("Error Conencting to database");
                          $_QUERY         = "SELECT * FROM tags WHERE tags.DONE = 0 GROUP BY VID";
                          $_RESULT        = mysqli_query($_CONNECTION, $_QUERY);
                          echo "
                          <div id=diary class = table-responsive>
                          <table class = 'table table-hover' id=\"diary_table\">
                            <thead>
                              <tr>
                                <th>Vehicle ID:</th>
                                <th>Date:</th>
                                <th>Jobs:</th>
                                <th>Tag ID:</th>
                                <th>Actions:</th>
                              </tr>
                            </thead>";
                          while($row = mysqli_fetch_array($_RESULT)){
                            ?>
                            <tbody>
                              <tr>
                                <!-- Vehicle ID -->
                                <td>
                                  <button class="btn btn-default btn-xs" name="button" id="VID_btn" disabled>
                                    <?php
                                      $_VEH_ID = $row['VID'];
                                      echo $row['VID'];
                                    ?>
                                  </button>
                                </td>
                                <!-- Date corresponding to the tag -->
                                <td>
                                  <?php
                                    $_DATE_QUERY = "SELECT DATE_CREATED FROM tags WHERE tags.VID = $_VEH_ID AND tags.DONE = 0";
                                    $_DATE_RESULT  = mysqli_query($_CONNECTION, $_DATE_QUERY);
                                    while($row_2 = mysqli_fetch_array($_DATE_RESULT)){
                                  ?>
                                        <button class="btn btn-default btn-xs" name="date_button" disabled><?php echo $row_2['DATE_CREATED']?></button><br>
                                  <?php
                                    }
                                  ?>
                                </td>
                                <!-- Tags -->
                                <td>
                                  <?php
                                    $_TAG_QUERY   = "SELECT DESCRIPTION,TAG_ID FROM tags WHERE tags.VID = $_VEH_ID AND tags.DONE = 0";
                                    $_TAG_RESULT  = mysqli_query($_CONNECTION, $_TAG_QUERY);
                                    while($row_3 = mysqli_fetch_array($_TAG_RESULT)){
                                  ?>
                                      <button class="btn btn-default btn-xs" name="desc_button" id="tag_des"><?php echo $row_3['DESCRIPTION']?></button><br>
                                  <?php
                                    }
                                  ?>
                                </td>
                                <!-- Tag ID for identification purposes only-->
                                <td>
                                  <?php
                                    $_TAG_ID_QUERY   = "SELECT TAG_ID FROM tags WHERE tags.VID = $_VEH_ID AND tags.DONE = 0";
                                    $_TAG_ID_RESULT  = mysqli_query($_CONNECTION, $_TAG_ID_QUERY);
                                    while($row_4 = mysqli_fetch_array($_TAG_ID_RESULT)){
                                  ?>
                                    <button class="btn btn-default btn-xs" name="desc_button" id="tag_des" disabled><?php echo $row_4['TAG_ID']?></button><br>
                                  <?php
                                    }
                                  ?>
                                </td>
                                <!-- Add button -->
                                <td>
                                  <button name="Add_des" id="Add_des" class ='btn btn-primary btn-xs'>Add Job</button>
                                </td>
                              </tr>
                            </tbody>
                          <?php
                            }
                          ?>
                        </table>
                      </div>
                    </div>
                    <button class = "btn btn-primary" id="new_entry">New Entry</button>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Scripts and shit -->
    <script src="../jquery/jquery.min.js"></script>
    <script src="../bootstrap/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../public/metisMenu/metisMenu.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/java_diary.js"></script>
  </body>
</html>
