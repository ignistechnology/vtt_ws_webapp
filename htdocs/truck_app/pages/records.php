<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style_new.css">
    <!-- Bootstrap link -->
    <link href="../bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Font -->
    <link href="../public/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- MetisMenu -->
    <link href="../public/metisMenu/metisMenu.min.css" rel="stylesheet">
    <style>
      label, input {display: block;}
      fieldset{padding:0; border:0; margin-top:25px;}
    </style>
    <title>Records Page</title>
  </head>
  <body>
  
    <!-- Navigation bars -->
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="navbar-header">
          <a class="navbar-brand" href="diary.php">Van Tonder Transport</a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
          <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-user">
                  <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
                  <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a></li>
                  <li class="divider"></li>
                  <li>
                    <a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                  </li>
              </ul>

          </li>
        </ul>
        <div class="navbar-default sidebar" role="navigation">
          <div class="sidebar-nav navbar-collapse">
              <ul class="nav" id="side-menu">
                <li>
                  <a href="admin.html"><i class="fa fa-dashboard fa-fw"></i> Admin</a>
                </li>
                <li>
                  <a href="diary.php"><i class="fa fa-table fa-fw"></i> Open Jobs</a>
                </li>
                <li>
                  <a href="#"><i class="fa fa-bar-chart-o fa-caret-down fa-fw" id="serv_rec"></i> Service Records</a>
                  <div class="drop_container">
                    <ul class="nav nav-second-level">
                      <li>
                        <a href="services.php"> Service</a>
                      </li>
                      <li>
                        <a href="maintenance.php"> Maintenance</a>
                      </li>
                      <li>
                        <a href="warranty.php"> Warranty</a>
                      </li>
                      <li>
                        <a href="fines.php"> Fines</a>
                      </li>
                    </ul>
                  </div>
                </li>
                <li>
                    <a href="tyres.php"><i class="fa fa-dot-circle-o fa-fw"></i> Tyres</a>
                </li>
                <li>
                    <a href="brakes.php"><i class="fa fa-ban fa-fw"></i> Brakes</a>
                </li>                                
              </ul>
          </div>
        </div>
      </nav>
    </div>

    <!-- Page Content -->
    <div class="page-wrapper">
        <div class="jumbotron text-center">
          <h1>Service Records Page</h1>
        </div>
      </div>
   <!-- Scripts and shit -->
    <script src="../jquery/jquery.min.js"></script>
    <script src="../bootstrap/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../public/metisMenu/metisMenu.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/java_diary.js"></script>
  </body>
</html>
